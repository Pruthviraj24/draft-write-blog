import { RichUtils,Entity,convertToRaw,AtomicBlockUtils} from "draft-js"
import {useState,useRef} from "react"
// import {PlusCircleOutlined,CloseCircleOutlined} from "@ant-design/icons"
import createVideoPlugin from '@draft-js-plugins/video';
import Editor, { createEditorStateWithText } from '@draft-js-plugins/editor';
import createSideToolbarPlugin from '@draft-js-plugins/side-toolbar';
// import editorStyles from './editorStyles.module.css';
import '@draft-js-plugins/side-toolbar/lib/plugin.css';
import { Button, Upload} from "antd";
// import {Dropdown, Menu, Space,Tooltip } from "antd"
// import BlockStyleControls from "./components/BlockStyleControls";
import createInlineToolbarPlugin from '@draft-js-plugins/inline-toolbar';
import '@draft-js-plugins/inline-toolbar/lib/plugin.css';
import './App.css';

import {
  HeadlineOneButton,
  HeadlineTwoButton,
  BlockquoteButton,
  CodeBlockButton,
  UnorderedListButton,
  OrderedListButton,
} from '@draft-js-plugins/buttons';

import {
  ItalicButton,
  BoldButton,
  UnderlineButton,
  CodeButton,
  AlignBlockCenterButton,
  AlignBlockLeftButton,
  AlignBlockRightButton,
  // AlignTextCenterButton,
  // AlignTextLeftButton,
  // AlignTextRightButton,
} from '@draft-js-plugins/buttons';





const sideToolbarPlugin = createSideToolbarPlugin();
const videoPlugin = createVideoPlugin();
const inlineToolbarPlugin = createInlineToolbarPlugin();
const { SideToolbar } = sideToolbarPlugin;
const {InlineToolbar} = inlineToolbarPlugin
const plugins = [sideToolbarPlugin,videoPlugin,inlineToolbarPlugin];



function App() {

  const [editorState, setEditorState] = useState(() =>
    createEditorStateWithText('')
  );


  const editor = useRef(null)

  const onChange = (editorState)=>{
    setEditorState(editorState)
  }


  const handleKeyCommand= (command, editorState)=>{
    const newState = RichUtils.handleKeyCommand(editorState,command)
    if(newState){
      onChange(newState)
      return true
    }
    return false
  }

  // const toggleBlockType = (blockType)=>{
  //   onChange(RichUtils.toggleBlockType(editorState,blockType))
  // }

  // const focus = () => {
  //   editor.current.focus();
  // };
  

  const publishBlog = ()=>{
    const JSBlogPost = { ...editorState, content: JSON.stringify(convertToRaw(editorState.getCurrentContent()))};
    console.log(JSBlogPost.content);
  }




  const addImage = (file)=>{
  
    const src = URL.createObjectURL(file);
    const contentState = editorState.getCurrentContent();
    const entityKey = contentState.createEntity('image', 'IMMUTABLE', {src});
    const newState = AtomicBlockUtils.insertAtomicBlock(editorState, entityKey, '');
    return newState;

  }

  const VideoAdd = ()=>{

    return(
      <div>
            <Upload
                beforeUpload={(file)=>{
                  addImage(file)
                  return false
                } }
                customRequest={false}
                showUploadList={false}
                multiple={true}
                listType="picture"
              >
                <Button
                  style={{ border: "none" }}
                  value="image"
                >
                  image
                </Button>
            </Upload>
      </div>
    )
  }

//   const insertImage = ( url) => {
//     const contentState = editorState.getCurrentContent();
//     const contentStateWithEntity = contentState.createEntity(
//         "IMAGE",
//         'IMMUTABLE',
//        {src: url}
//   )
// const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
// const newEditorState = setEditorState( editorState, { currentContent: contentStateWithEntity });
// return AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, ' ');
// };



  return (
    <>
    <div className="main-container editor">

          <Editor
            blockStyleFn={getBlockStyle}
            editorState={editorState}
            handleKeyCommand={handleKeyCommand}
            placeholder = "Tell Us Story"
            ref={(element) => {
              editor.current = element;
            }}
            spellCheck={true}
            onChange={onChange}
            id="main-editor"
            blockRendererFn={mediaBlockRenderer}
            plugins={plugins}
          />
        </div>
          <SideToolbar>
          {
            (externalProps) => (
              <div>
                <HeadlineOneButton {...externalProps} />
                <HeadlineTwoButton {...externalProps} />
                <BlockquoteButton {...externalProps} />
                <CodeBlockButton {...externalProps} />
                <UnorderedListButton {...externalProps}/>
                <OrderedListButton {...externalProps}/>
                <VideoAdd {...externalProps}/>
              </div>
            )
          }
          </SideToolbar>
          <InlineToolbar>
          {
            (externalProps) => (
              <div>
                <BoldButton {...externalProps} />
                <ItalicButton {...externalProps} />
                <UnderlineButton {...externalProps} />
                <CodeButton   {...externalProps}/>
                    <AlignBlockCenterButton  {...externalProps}/>
                    <AlignBlockLeftButton {...externalProps}/>
                    <AlignBlockRightButton {...externalProps}/>
                {/* <AlignTextCenterButton  {...externalProps}/>
                <AlignTextLeftButton   {...externalProps}/>
                <AlignTextRightButton  {...externalProps}/> */}
              </div>
            )
          }
          </InlineToolbar>
    <button className="publish-button" onClick={publishBlog}>Publish</button>
    </>
  );
}

export default App;



function getBlockStyle(block){
  switch(block.getType()){
    case 'blockquote' : return 'RichEditor-blockquote';
    default: return null;
  }
}





// const styleMap = [
//   {
//     value: 'Bold',
//     style: 'BOLD'
//   },

//   {
//     value: 'Italic',
//     style: 'ITALIC'
//   },

//   {
//     value: 'Underline',
//     style: 'UNDERLINE'
//   },

//   {
//     value: 'Strikethrough',
//     style: 'STRIKETHROUGH'
//   },

//    {
//     value: 'Code',
//     style: 'CODE'
//   },

//   {
//     value: 'Highlight',
//     style: 'HIGHLIGHT'
//   }
// ];


function mediaBlockRenderer(block) {
	if (block.getType() === 'atomic') {
		return {
			component: Media,
			editable: false
		};
	}
	return null;
}

const Image = (props) => {
	return <img src={props.src} alt={props.name}/>;
};

const Media = (props) => {

	const entity = Entity.get(props.block.getEntityAt(0));

	const {src} = entity.getData();
	const type = entity.getType();

	let media;
	if (type === 'image') {
		media = <Image src={src} />;
	}

	return media;
}