import "./../App.css"


const StyleButton = (props)=>{
    const {label,onToggle,style} = props
    

    const toggleStyle = (event)=>{
        event.preventDefault()
        onToggle(style)
    }

    let className = 'RichEditor-styleButton';
    if (props.active) {
      className += ' RichEditor-activeButton';
    }

    return(
        <button className={className} onMouseDown={toggleStyle}>
            {label}
        </button>
    )
}

export default StyleButton