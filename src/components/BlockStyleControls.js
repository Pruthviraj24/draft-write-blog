import StyleButton from "./StyleButton";
import {UnorderedListOutlined,OrderedListOutlined} from "@ant-design/icons"
import {BiHeading, BiCodeAlt} from "react-icons/bi"
import {ImQuotesLeft} from "react-icons/im"
import {v4 as uuid} from "uuid"
import "./../App.css"


const BLOCK_TYPES = [
    {label: <BiHeading/>, style:"header-one",id:uuid()},
    {label: 'H2', style: 'header-two',id:uuid()},
    {label: 'H3', style: 'header-three',id:uuid()},
    {label: <ImQuotesLeft/>, style: 'blockquote',id:uuid()},
    {label: <UnorderedListOutlined />, style: 'unordered-list-item',id:uuid()},
    {label: <OrderedListOutlined />, style: 'ordered-list-item',id:uuid()},
    {label: <BiCodeAlt/>, style: 'code-block',id:uuid()},
    {label: "Video", style: 'video',id:uuid()},
    {label: "Image", style: 'image',id:uuid()},
    {label: "GIF", style: 'gif',id:uuid()},
  ];

const BlockStyleControls = (props) => {
    const {editorState} = props;
    const selection = editorState.getSelection();
    const blockType = editorState
      .getCurrentContent()
      .getBlockForKey(selection.getStartKey())
      .getType();
    
 
    return (
      <div className="RichEditor-controls">
        {BLOCK_TYPES.map((type) =>
          <StyleButton
            key={type.id}
            active={type.style === blockType}
            label={type.label}
            onToggle={props.onToggle}
            style={type.style}
          />
        )}
      </div>
    );
  };

export default BlockStyleControls